package com.netty.websocket;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;


public class WebSocketChannelInitializer extends ChannelInitializer<SocketChannel> {

    private String ip;

    private Integer port;


    public WebSocketChannelInitializer(String ip,Integer port){
        this.ip=ip;
        this.port=port;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();

        //websocket协议本身是基于http协议的，所以这边也要使用http解编码器
        pipeline.addLast(new HttpServerCodec());

        pipeline.addLast(new HttpObjectAggregator(63356));

        //以块的方式来写的处理器
        pipeline.addLast(new ChunkedWriteHandler());

        //websocket定义了传递数据的6中frame类型
        pipeline.addLast(new MyWebSocketServerHandler(ip,port));
    }
}